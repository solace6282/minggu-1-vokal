package com.example.vokal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var input : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        submit.setOnClickListener {
            countVowel()
        }


    }

    fun getText(){
        input = inputtext.text.toString()
    }

    fun countVowel(){
        getText()
        val vowel : MutableList<Char> = mutableListOf()
        for(i in input){
            if((i == 'a' || i == 'e' || i == 'i' || i == 'o' || i == 'u') && !vowel.contains(i)){
                vowel.add(i)
            }
        }
        var cnt = vowel.size
        result.text = "Vokal: $cnt"
    }
}